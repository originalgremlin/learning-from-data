#!/usr/bin/env node

var pow = Math.pow,
    E = Math.E;

var eta = 0.1;
var update = function (w, dw) {
    for (var i = 0, len = w.length; i < len; i++)
        w[i] -= eta * dw[i];
};

var gradient = function (w) {
    var u = w[0],
        v = w[1],
        du = 2 * (u * pow(E, v) - 2 * v * pow(E, -u)) * (pow(E, v) + 2 * v * pow(E, -u)),
        dv = 2 * (u * pow(E, v) - 2 * v * pow(E, -u)) * (u * pow(E, v) - 2 * pow(E, -u));
        return [du, dv];
};

var error = function (w) {
    var u = w[0],
        v = w[1];
    return Math.pow(u * pow(E, v) - 2 * v * pow(E, -u), 2);
};


var epsilon = 1e-14,
    w = [1.0, 1.0],
    err = error(w),
    iterations = 0;
while (true) {
    console.log(iterations, w, err);
    if (err < epsilon)
        break;
    update(w, gradient(w));
    err = error(w);
    iterations++;
}
