#!/usr/bin/env node

// return point as [1.0, -1.0 <= x < +1.0, -1.0 <= y < +1.0]
var getPoint = function () {
    var x = 2 * Math.random() - 1,
        y = 2 * Math.random() - 1;
    return [1.0, x, y];
};

var getPoints = function (N) {
    var rv = new Array(N);
    for (var i = 0; i < N; i++)
        rv[i] = getPoint();
    return rv;
};

// +1 if the point is above the line
// -1 otherwise
var getExpectedValue = function (f, p) {
    return f[0] * p[1] + f[1] < p[2] ? +1 : -1;
};

// target function is a random line defined by p0 and p1
var getTargetFunction = function () {
    var p0 = getPoint(),
        p1 = getPoint(),
        // slope
        m = (p0[2] - p1[2]) / (p0[1] - p1[1]),
        // offset
        b = p0[2] - m * p0[1];
        // y = mx + b
        return [m, b];
};

var diff = function (w0, w1) {
    var sum = 0.0;
    for (var i = 0, len = w0.length; i < len; i++)
        sum += Math.pow(w1[i] - w0[i], 2);
    return Math.sqrt(sum);
};

var swap = function (array, i, j) {
    var tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
    return array;
};

var shuffle = function (array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor((i + 1) * Math.random());
        swap(array, i, j);
    }
    return array;
};

var scalarmult = function (vec, scalar) {
    return vec.map(function (x) {
        return x * scalar;
    });
};

var vecmult = function (v1, v2) {
    var len = v1.length,
        rv = 0.0;
    for (var i = 0; i < len; i++)
        rv += v1[i] * v2[i];
    return rv;
};

var vecadd = function (v1, v2) {
    var len = v1.length,
        rv = new Array(len);
    for (var i = 0; i < len; i++)
        rv[i] = v1[i] + v2[i];
    return rv;
};

var copy = function (arr) {
    return arr.map(function (x) { return x; });
};

var crossEntropyError = function (xs, ys, w) {
    var rv = 0.0,
        N = xs.length;
    for (var i = 0; i < N; i++)
        rv += Math.log(1 + Math.pow(Math.E, -ys[i] * vecmult(w, xs[i])));
    return rv / N;
};

var iterations = 100,
    eoutsum = 0.0,
    epochsum = 0;
for (var j = 1; j <= iterations; j++) {
    var N = 100,
        f = getTargetFunction(),
        xs = getPoints(N),
        ys = [],
        w0 = [0, 0, 0],
        w1 = [0, 0, 0],
        gradient = [0, 0, 0],
        eta = 0.01,
        epsilon = 0.01,
        epochs = 0;

    do {
        // shuffle the inputs
        shuffle(xs);
        ys = xs.map(getExpectedValue.bind(null, f));
        w0 = copy(w1);
        // do one epoch of gradient descent
        for (var i = 0; i < N; i++) {
            gradient = scalarmult(xs[i], -ys[i] / (1 + Math.pow(Math.E, ys[i] * vecmult(w1, xs[i]))));
            w1 = vecadd(w1, scalarmult(gradient, -eta));
        }
        epochs++;
    } while (diff(w0, w1) > epsilon);

    var xout = getPoints(1000),
        yout = xout.map(getExpectedValue.bind(null, f)),
        eout = crossEntropyError(xout, yout, w1);
    console.log(j, epochs, eout);

    eoutsum += eout;
    epochsum += epochs;
}

console.log(eoutsum / iterations, epochsum / iterations);
