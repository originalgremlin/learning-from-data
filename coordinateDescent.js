#!/usr/bin/env node

var pow = Math.pow,
    E = Math.E;

var du = function (w) {
    var u = w[0],
        v = w[1],
        du = 2 * (u * pow(E, v) - 2 * v * pow(E, -u)) * (pow(E, v) + 2 * v * pow(E, -u));
    return du;
};

var dv = function (w) {
    var u = w[0],
        v = w[1],
        dv = 2 * (u * pow(E, v) - 2 * v * pow(E, -u)) * (u * pow(E, v) - 2 * pow(E, -u));
    return dv;
};

var error = function (w) {
    var u = w[0],
        v = w[1];
    return Math.pow(u * pow(E, v) - 2 * v * pow(E, -u), 2);
};

var w = [1.0, 1.0],
    eta = 0.1;
for (var i = 0; i <= 15; i++) {
    console.log(i, w, error(w));
    w[0] -= eta * du(w);
    w[1] -= eta * dv(w);
}
