function err = originalVC(Dvc, d, N)
    err = sqrt(8/N * (log(4) + (Dvc * log(2 * N)) - log(d)));
end

function err = rademacher(Dvc, d, N)
    err = sqrt((2/N) * (log(2*N) + (Dvc * log(N)))) + sqrt((2/N) * log(1/d)) + 1/N;
end

function err = parrondo(Dvc, d, N)
    err = sqrt(1/N * (2/e + log(6) + (Dvc * log(2 * N)) - log(d)));
end

function err = devroye(Dvc, d, N)
    err = sqrt(1/(2*N) * ((4*e + 4*e*e) + (log(4) + (Dvc * log(N * N)) - log(d))));
end
