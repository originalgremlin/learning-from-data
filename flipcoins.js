#!/usr/bin/env node

var P = 100000,
    N = 1000,
    M = 10;

var coins = new Array(N);

var first = 0,
    rand = 0,
    min = 0;

for (var p = 0; p < P; p++) {
    for (var n = 0; n < N; n++) {
        coins[n] = 0;
        for (var m = 0; m < M; m++)
            if (Math.random() < 0.5)
                coins[n]++;
    }
    first += coins[0];
    rand += coins[Math.floor(N * Math.random())];
    min += Math.min.apply(null, coins);
}

var total = M * P;
console.log(first / total, rand / total, min / total);
