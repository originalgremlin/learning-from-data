#!/usr/bin/env node

var gbar = function (N) {
    var sum = 0.0,
        x1, y1, x2, y2;
    for (var i = 0; i < N; i++) {
        x1 = 2 * Math.random() - 1.0;
        y1 = Math.sin(Math.PI * x1);
        x2 = 2 * Math.random() - 1.0;
        y2 = Math.sin(Math.PI * x2);
        sum += (x1 * x1 * y1 + x2 * x2 * y2) / (x1 * x1 * x1 * x1 + x2 * x2 * x2 * x2); // ax^2
    }
    return sum / N;
};

var bias = function (N, g) {
    var sum = 0.0,
        x, gx, fx;
    for (var i = 0; i < N; i++) {
        x = 2 * Math.random() - 1.0;
        gx = g * x * x; // ax^2
        fx = Math.sin(Math.PI * x);
        sum += Math.pow(gx - fx, 2);
    }
    return sum / N;
};

var variance = function (N, g) {
    var sum = 0.0,
        x1, y1, x2, y2, xout, gx;
    for (var i = 0; i < N; i++) {
        x1 = 2 * Math.random() - 1.0;
        y1 = Math.sin(Math.PI * x1);
        x2 = 2 * Math.random() - 1.0;
        y2 = Math.sin(Math.PI * x2);
        gx = (x1 * x1 * y1 + x2 * x2 * y2) / (x1 * x1 * x1 * x1 + x2 * x2 * x2 * x2); // ax^2
        for (var j = 0; j < 100; j++) {
            xout = 2 * Math.random() - 1.0;
            sum += Math.pow((gx * xout * xout) - (g * xout * xout), 2); // ax^2
        }
    }
    return sum / (N * 100);
};

g = gbar(1e5);
b = bias(1e5, g);
v = variance(1e5, g);
eout = b + v;
console.log({
    'gbar': g,
    'bias': b,
    'variance': v,
    'eout': eout
});
