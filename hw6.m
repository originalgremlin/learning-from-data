clear;
close all;

function [phi] = dataTransform(X, y)
    m = length(X);
    % parse inputs
    x1 = X(:, 1);
    x2 = X(:, 2);
    % transform paramters
    phi = [ones(m, 1) x1 x2 x1.**2 x2.**2 x1.*x2 abs(x1.-x2) abs(x1.+x2)];
end

% load training and test set
data_in = load('hw6_in.txt');
X_in = data_in(:, [1 2]);
y_in = data_in(:, 3);

data_out = load('hw6_out.txt');
X_out = data_out(:, [1 2]);
y_out = data_out(:, 3);

% transform to non-linear data
phi_in = dataTransform(X_in, y_in);
phi_out = dataTransform(X_out, y_out);

% run linear regression
w_lin = pinv(phi_in) * y_in;

% compute sample errors of linear regression
Ein = mean(sign(phi_in * w_lin) != y_in);
Eout = mean(sign(phi_out * w_lin) != y_out);
display([Ein Eout]);

% compute sample errors of linear regression with regularization
w_reg = inv((phi_in' * phi_in) + (10**-3 * eye(size(phi_in, 2)))) * phi_in' * y_in;
Ein = mean(sign(phi_in * w_reg) != y_in);
Eout = mean(sign(phi_out * w_reg) != y_out);
display([-3 Ein Eout]);

w_reg = inv((phi_in' * phi_in) + (10**-2 * eye(size(phi_in, 2)))) * phi_in' * y_in;
Ein = mean(sign(phi_in * w_reg) != y_in);
Eout = mean(sign(phi_out * w_reg) != y_out);
display([-2 Ein Eout]);

w_reg = inv((phi_in' * phi_in) + (10**-1 * eye(size(phi_in, 2)))) * phi_in' * y_in;
Ein = mean(sign(phi_in * w_reg) != y_in);
Eout = mean(sign(phi_out * w_reg) != y_out);
display([-1 Ein Eout]);

w_reg = inv((phi_in' * phi_in) + (10**0 * eye(size(phi_in, 2)))) * phi_in' * y_in;
Ein = mean(sign(phi_in * w_reg) != y_in);
Eout = mean(sign(phi_out * w_reg) != y_out);
display([0 Ein Eout]);

w_reg = inv((phi_in' * phi_in) + (10**1 * eye(size(phi_in, 2)))) * phi_in' * y_in;
Ein = mean(sign(phi_in * w_reg) != y_in);
Eout = mean(sign(phi_out * w_reg) != y_out);
display([1 Ein Eout]);

w_reg = inv((phi_in' * phi_in) + (10**2 * eye(size(phi_in, 2)))) * phi_in' * y_in;
Ein = mean(sign(phi_in * w_reg) != y_in);
Eout = mean(sign(phi_out * w_reg) != y_out);
display([2 Ein Eout]);

w_reg = inv((phi_in' * phi_in) + (10**3 * eye(size(phi_in, 2)))) * phi_in' * y_in;
Ein = mean(sign(phi_in * w_reg) != y_in);
Eout = mean(sign(phi_out * w_reg) != y_out);
display([3 Ein Eout]);
