clear;
close all;

function [phi] = dataTransform(X, y)
    m = length(X);
    % parse inputs
    x1 = X(:, 1);
    x2 = X(:, 2);
    % transform paramters
    phi = [ones(m, 1) x1 x2 x1.**2 x2.**2 x1.*x2 abs(x1.-x2) abs(x1.+x2)];
end

% load training and test set
data_in = load('hw6_in.txt');
X_in = data_in(:, [1 2]);
y_in = data_in(:, 3);

data_out = load('hw6_out.txt');
X_out = data_out(:, [1 2]);
y_out = data_out(:, 3);

% transform to non-linear data
phi_in = dataTransform(X_in, y_in);
phi_out = dataTransform(X_out, y_out);

for i = 4:8
    % cut input and output down to a smaller polynomial model
    phi_train = phi_in(26:35,1:i);
    phi_validate = phi_in(1:25,1:i);
    phi_out_k = phi_out(:,1:i);

    y_train = y_in(26:35);
    y_validate = y_in(1:25);

    % run linear regression on training set
    w = pinv(phi_train) * y_train;

    % compute sample errors of linear regression
    Ein = mean(sign(phi_train * w) != y_train);
    Eval = mean(sign(phi_validate * w) != y_validate);
    Eout = mean(sign(phi_out_k * w) != y_out);

    % display results
    printf('K    = %d\n', i - 1);
    printf('Ein  = %f\n', Ein);
    printf('Eval = %f\n', Eval);
    printf('Eout = %f\n\n', Eout);
end
