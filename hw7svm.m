clear;
close all;

addpath('~/bin/libsvm');

function [y] = getValues (X, f)
    m = size(X, 1);
    y = zeros(m, 1);
    % calculate per-element cross-product
    dx = f(2, 1) - f(1, 1);
    dy = f(2, 2) - f(1, 2);
    for i = 1:m
        y(i) = sign((dx * (X(i, 2) - f(1, 2))) - (dy * (X(i, 1) - f(1, 1))));
    end
end

iterations = 0;
n = 100;
nout = 1000;
numsv = 0;
epsilon = 1e-3;
Esvmsum = 0;
Eplasum = 0;
svmwins = 0;
while (iterations < 1000)
    f = 2 * rand(2, 2) - 1;
    X = 2 * rand(n, 2) - 1;
    y = getValues(X, f);

    % If all data points are on one side of the line, discard the run and start a new run.
    if (~any(y == 1) || all(y == 1))
        continue
    end

    % SVM
    model = svmtrain(y, X, '-s 0 -t 0 -c 1e6 -q');
    wsvm = model.SVs' * model.sv_coef;
    b = -model.rho;
    if (model.Label(1) == -1)
        wsvm = -wsvm;
        b = -b;
    end
    numsv += sum(abs((y .* (X * wsvm + b)) - 1.0) < epsilon);

    % PLA
    % add x0 parameter
    X = [ones(n, 1) X];
    % start PLA with the all-zero vector
    wpla = zeros(1, 3);
    while (true)
        misclassified = find(sign(X * wpla') ~= y);
        if (length(misclassified) == 0)
            break
        else
            mindex = misclassified(randi(length(misclassified)));
            xmis = X(mindex, :);
            ymis = y(mindex);
            wpla += xmis * ymis;
        end
    end

    % calculate out of sample errors for the two methods
    Xout = 2 * rand(nout, 2) - 1;
    yout = getValues(Xout, f);
    Xout = [ones(nout, 1) Xout];
    % SVM
    Esvm = sum(sign(Xout * [b; wsvm]) ~= yout) / nout;
    Esvmsum += Esvm;
    % PLA
    Epla = sum(sign(Xout * wpla') ~= yout) / nout;
    Eplasum += Epla;
    % which is better?
    if (Esvm < Epla)
        svmwins++;
    end

    % advance the loop
    iterations++;
end
printf('N = %d\n', n);
printf('Iterations = %d\n', iterations);
printf('Epla = %f\n', Eplasum / iterations);
printf('Esvm = %f\n', Esvmsum / iterations);
printf('Num SV = %f\n', numsv / iterations);
printf('SVM wins = %f\n', svmwins / iterations);
