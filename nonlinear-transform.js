#!/usr/bin/env node

var sylvester = require('sylvester');

// return point as [1.0, -1.0 <= x < +1.0, -1.0 <= y < +1.0]
var getPoint = function () {
    var x = 2 * Math.random() - 1,
        y = 2 * Math.random() - 1;
    return [1.0, x, y];
};

var getPoints = function (N) {
    var rv = new Array(N);
    for (var i = 0; i < N; i++)
        rv[i] = getPoint();
    return rv;
};


// linear regression utils
var h = function (w, p) {
    var sum = 0.0;
    for (var i = 0, len = w.length; i < len; i++)
        sum += w[i] * p[i];
    return Math.sign(sum);
};

var LRError = function (h, A, w, f) {
    var N = A.length,
        misclassified = 0;
    for (var i = 0; i < N; i++)
        if (h(w, A[i]) !== f(A[i]))
            misclassified++;
    return misclassified / N;
};

var f = function (p) {
    return Math.sign(p[1] * p[1] + p[2] * p[2] - 0.6);
};

var fnoise = function (p) {
    return Math.random() >= 0.1 ? f(p) : f(p) * -1;
};

var dataTransform = function (p) {
    var x1 = p[1],
        x2 = p[2];
    return [1, x1, x2, x1 * x2, x1 * x1, x2 * x2];
};

// run experiment
var Ein = 0.0,
    Eout = 0.0;
    wtotal = [0, 0, 0, 0, 0, 0];
for (var i = 0; i < 1000; i++) {
    var A = $M(getPoints(1000).map(dataTransform)),
        b = $V(A.elements.map(fnoise)),
        pinv = (((A.transpose()).multiply(A)).inverse()).multiply(A.transpose()),
        w = pinv.multiply(b).elements;

    for (var j = 0; j < 6; j++)
        wtotal[j] += w[j];

    Ein += LRError(h, A.elements, w, f);
    Eout += LRError(h, getPoints(1000).map(dataTransform), w, fnoise);
}
for (var j = 0; j < 6; j++)
    wtotal[j] /= 1000;
console.log(wtotal);
console.log(Ein / 1000);
console.log(Eout / 1000);
