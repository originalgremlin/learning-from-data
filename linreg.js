#!/usr/bin/env node

var sylvester = require('sylvester');

// return point as [1.0, -1.0 <= x < +1.0, -1.0 <= y < +1.0]
var getPoint = function () {
    var x = 2 * Math.random() - 1,
        y = 2 * Math.random() - 1;
    return [1.0, x, y];
};

var getPoints = function (N) {
    var rv = new Array(N);
    for (var i = 0; i < N; i++)
        rv[i] = getPoint();
    return rv;
};

// +1 if the point is above the line
// -1 otherwise
var getExpectedValue = function (f, p) {
    return f[0] * p[1] + f[1] < p[2] ? +1 : -1;
};

// target function is a random line defined by p0 and p1
var getTargetFunction = function () {
    var p0 = getPoint(),
        p1 = getPoint(),
        // slope
        m = (p0[2] - p1[2]) / (p0[1] - p1[1]),
        // offset
        b = p0[2] - m * p0[1];
        // y = mx + b
        return [m, b];
};

// linear regression utils
/*
var h = function (w, p) {
    var sum = 0.0;
    for (var i = 0; i < 3; i++)
        sum += w[i] * p[i];
    return sum;
};

var LRError = function (h, A, w, f) {
    var N = A.length,
        misclassified = 0;
    for (var i = 0; i < N; i++)
        if (Math.sign(h(w, A[i])) !== getExpectedValue(f, A[i]))
            misclassified++;
    return misclassified / N;
};

// run experiment
var Ein = 0.0,
    Eout = 0.0;
for (var i = 0; i < 1000; i++) {
    var f = getTargetFunction(),
        A = getPoints(100),
        b = A.map(getExpectedValue.bind(null, f));

    var Amat = $M(A),
        bvec = $V(b),
        pinv = (((Amat.transpose()).multiply(Amat)).inverse()).multiply(Amat.transpose()),
        w = pinv.multiply(bvec);

    Ein += LRError(h, A, w.elements, f);
    Eout += LRError(h, getPoints(1000), w.elements, f);
}
console.log(Ein / 1000);
console.log(Eout / 1000);
*/

// run perceptron algorithm
var h = function (w, p) {
    var sum = 0;
    for (var i = 0; i < 3; i++)
        sum += w[i] * p[i];
    return sum >= 0 ? +1 : -1;
};

var iterations = 0;
for (var z = 0; z < 1000; z++) {
    var f = getTargetFunction(),
        A = getPoints(10),
        b = A.map(getExpectedValue.bind(null, f));

    var Amat = $M(A),
        bvec = $V(b),
        pinv = (((Amat.transpose()).multiply(Amat)).inverse()).multiply(Amat.transpose()),
        w = pinv.multiply(bvec).elements;

    var hasUnexpectedValue = function (p) {
        return h(w, p) !== getExpectedValue(f, p);
    };

    while (true) {
        iterations++;
        // find misclassified points
        var misclassified = A.filter(hasUnexpectedValue);
        // no misclassified points.  we're done.
        if (misclassified.length === 0)
            break;
        // update the perceptron weights
        var xmis = misclassified[Math.floor(misclassified.length * Math.random())],
            ymis = getExpectedValue(f, xmis);
        for (var i = 0; i < 3; i++)
            w[i] += ymis * xmis[i];
    }
}

console.log(iterations / 1000);
